api = 2
core = 7.x

; Libraries
libraries[htmlpurifier][directory_name] = coingate-php
libraries[htmlpurifier][download][type] = file
libraries[htmlpurifier][download][url] = https://github.com/coingate/coingate-php/archive/v3.0.1.tar.gz
libraries[htmlpurifier][type] = library
